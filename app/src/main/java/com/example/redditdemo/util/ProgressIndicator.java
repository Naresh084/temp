package com.example.redditdemo.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import com.example.redditdemo.R;

public class ProgressIndicator {
    private Dialog progressDialog;

    public ProgressIndicator(Context context) {
        this.progressDialog = progressDialog;
        progressDialog = new Dialog(context);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setContentView(R.layout.app_progressbar);

    }

    public void showProgress() {
        progressDialog.show();
    }

    public void stopProgress() {
        progressDialog.hide();
    }

}
