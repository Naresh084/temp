package com.example.redditdemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;


import com.example.redditdemo.Database.DatabaseHandler;
import com.example.redditdemo.WebActivity;
import com.example.redditdemo.R;
import com.example.redditdemo.model.PostModel;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.CustomViewHolder> {

     private Context context;
     private ArrayList<PostModel> postModelArrayList;
     private DatabaseHandler databaseHandler;
     private static int VIEW_TYPE_ITEM=101,VIEW_TYPE_LOADING=102;

    class CustomViewHolder extends RecyclerView.ViewHolder {

        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }
    class MyViewHolder extends CustomViewHolder {
        private TextView txtTitle,txtCategory,txtDomain,txtTotalUps,txtComments,txtSelfText;
        private ImageView imgData,imgShare;
        private CheckBox ckLike;

        MyViewHolder(View view) {
            super(view);
            try {
                txtTitle = (TextView) view.findViewById(R.id.title);
                txtCategory = (TextView) view.findViewById(R.id.category);
                txtDomain = (TextView) view.findViewById(R.id.domain);
                txtTotalUps = (TextView) view.findViewById(R.id.totalLikes);
                txtComments = (TextView) view.findViewById(R.id.totalComments);
                txtSelfText = (TextView) view.findViewById(R.id.textPost);
                imgData = (ImageView) view.findViewById(R.id.imagePost);
                ckLike = (CheckBox) view.findViewById(R.id.imgAddFavorite);
                imgShare = (ImageView) view.findViewById(R.id.imgShare);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    class ProgressViewHolder extends CustomViewHolder {

        public ProgressViewHolder(View itemView) {
            super(itemView);
        }
    }


    public PostAdapter(Context context,ArrayList<PostModel> postModels) {
        this.context = context;
        this.postModelArrayList = postModels;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = null;
        if (viewType == VIEW_TYPE_ITEM) {
            root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
            return new MyViewHolder(root);
        } else {
            root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
            return new ProgressViewHolder(root);
        }
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final PostModel postModel = postModelArrayList.get(position);
        try {
            if (holder instanceof MyViewHolder) {
                ((MyViewHolder) holder).txtTitle.setText(postModel.getTitle());
                ((MyViewHolder) holder).txtCategory.setText(postModel.getSubReddit());
                String total = "";

                if((Long.parseLong(postModel.getSubscribers()) >= 1000000))
                    total = (Long.parseLong(postModel.getSubscribers())/1000000)+"M";
                else if((Long.parseLong(postModel.getSubscribers()) >= 1000))
                    total = (Long.parseLong(postModel.getSubscribers())/1000)+"K";
                else
                    total = postModel.getSubscribers();
                if (postModel.getDomain() != null && !postModel.getDomain().trim().equalsIgnoreCase("")) {
                    ((MyViewHolder) holder).txtDomain.setText("-" + postModel.getSubReddit() + "- " + total+ " Subscribers");
                } else {
                    ((MyViewHolder) holder).txtDomain.setText("- " + total +" Subscribers");
                }

                total = "";
                if(postModel.getUps() >= 1000000)
                    total = ((postModel.getUps())/1000000)+"M";
                else if(postModel.getUps() >= 1000)
                    total = ((postModel.getUps())/1000)+"K";
                else
                    total = String.valueOf(postModel.getUps());

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String dateString = formatter.format(new Date((long)postModel.getCreated()));
                Log.d("data",dateString);


                ((MyViewHolder) holder).txtTotalUps.setText(total);
                ((MyViewHolder) holder).txtComments.setText(String.valueOf(postModel.getNumComments()) + " comments");

                if (!postModel.getSelfText().equalsIgnoreCase("")) {
                    ((MyViewHolder) holder).txtSelfText.setVisibility(View.VISIBLE);
                    ((MyViewHolder) holder).txtSelfText.setText(postModel.getSelfText());
                } else {
                    ((MyViewHolder) holder).txtSelfText.setVisibility(View.GONE);
                }
                if (!postModel.getThumbnail().equalsIgnoreCase("") && postModel.getThumbnail().contains("http")) {
                    ((MyViewHolder) holder).imgData.setVisibility(View.VISIBLE);
                    Picasso.get().load(postModel.getThumbnail()).into(((MyViewHolder) holder).imgData);
                } else {
                    ((MyViewHolder) holder).imgData.setVisibility(View.GONE);
                }

                if (postModel.isFavorite()) {
                    ((MyViewHolder) holder).ckLike.setChecked(true);
                } else {
                    ((MyViewHolder) holder).ckLike.setChecked(false);
                }

                ((MyViewHolder) holder).ckLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            if (databaseHandler.addFavorite(postModel)) {
                                Toast.makeText(context, "Post favorited successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Post already in favorite", Toast.LENGTH_SHORT).show();
                            }
                            ((MyViewHolder) holder).ckLike.setChecked(true);
                        } else {
                            if (databaseHandler.deleteFavorite(postModel.getId()) > 0) {
                                Toast.makeText(context, "Post unfavorited successfully", Toast.LENGTH_SHORT).show();
                                postModelArrayList.remove(position);
                                notifyDataSetChanged();
                            }
                            ((MyViewHolder) holder).ckLike.setChecked(false);
                        }
                    }
                });
            }

            ((MyViewHolder) holder).imgShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "https://redd.it/"+postModel.getId());
                    context.startActivity(Intent.createChooser(shareIntent, "Share link using"));
                }
            });
            ((MyViewHolder) holder).txtTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, WebActivity.class);
                    i.putExtra("link", postModel.getUrl());
                    i.putExtra("title", postModel.getTitle());
                    context.startActivity(i);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (postModelArrayList.get(position) != null)
            return VIEW_TYPE_ITEM;
        else
            return VIEW_TYPE_LOADING;
    }

    @Override
    public int getItemCount() {
//        return valveAreaDetailsModelList.size();
        return postModelArrayList.size();
    }

}