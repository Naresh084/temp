package com.example.redditdemo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostModel implements Serializable {
    private int dbId=0;
    private String dbTime="";


    @SerializedName("subreddit")
    private String subReddit;
    @SerializedName("title")
    private String title;
    @SerializedName("selftext")
    private String selfText;
    @SerializedName("ups")
    private long ups;
    @SerializedName("score")
    private long score;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("created")
    private double created;
    @SerializedName("domain")
    private String domain;
    @SerializedName("num_comments")
    private String numComments;
    @SerializedName("url")
    private String url;
    @SerializedName("id")
    private String id;
    @SerializedName("subreddit_subscribers")
    private String subscribers;

    private boolean isFavorite= false;

    public String getSubReddit() {
        return subReddit;
    }

    public int getDbId() {
        return dbId;
    }

    public String getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(String subscribers) {
        this.subscribers = subscribers;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public String getDbTime() {
        return dbTime;
    }

    public void setDbTime(String dbTime) {
        this.dbTime = dbTime;
    }

    public void setSubReddit(String subReddit) {
        this.subReddit = subReddit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSelfText() {
        return selfText;
    }

    public void setSelfText(String selfText) {
        this.selfText = selfText;
    }

    public long getUps() {
        return ups;
    }

    public void setUps(long ups) {
        this.ups = ups;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public double getCreated() {
        return created;
    }

    public void setCreated(double created) {
        this.created = created;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getNumComments() {
        return numComments;
    }

    public void setNumComments(String numComments) {
        this.numComments = numComments;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public String toString() {
        return "PostModel{" +
                "dbId=" + dbId +
                ", dbTime='" + dbTime + '\'' +
                ", subReddit='" + subReddit + '\'' +
                ", title='" + title + '\'' +
                ", selfText='" + selfText + '\'' +
                ", ups=" + ups +
                ", score=" + score +
                ", thumbnail='" + thumbnail + '\'' +
                ", created=" + created +
                ", domain='" + domain + '\'' +
                ", numComments='" + numComments + '\'' +
                ", url='" + url + '\'' +
                ", id='" + id + '\'' +
                ", isFavorite=" + isFavorite +
                '}';
    }
}
