package com.example.redditdemo;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.example.redditdemo.storage.SharedPreferenceUtil;
import com.example.redditdemo.storage.SharedPreferencesTokens;
import com.jacksonandroidnetworking.JacksonParserFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by Trushit on 12/01/16.
 */
public class ApplicationClass extends Application {
    /**
     * Global request queue for Volley
     */
    /**
     * Log or request TAG
     */
    public static final String TAG = "VolleyPatterns";

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static ApplicationClass sInstance;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;

        // initialize the singleton
        sInstance = this;
        SharedPreferenceUtil.init(this);
        SharedPreferencesTokens.init(this);
        // DatabaseUtil.init(this, getString(R.string.dbname), 1, null);
        AndroidNetworking.initialize(getApplicationContext());

        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.HEADERS);
        AndroidNetworking.setParserFactory(new JacksonParserFactory());
        generateKey();

    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    private void generateKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.example.redditdemo", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("APPLICATION_CLASS", "Keyhash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized ApplicationClass getInstance() {
        return sInstance;
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */


}
