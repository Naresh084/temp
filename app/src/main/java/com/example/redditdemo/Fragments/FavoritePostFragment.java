package com.example.redditdemo.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.example.redditdemo.Database.DatabaseHandler;
import com.example.redditdemo.R;
import com.example.redditdemo.adapter.PostAdapter;
import com.example.redditdemo.model.PostModel;
import com.example.redditdemo.util.NetworkUtil;
import com.example.redditdemo.util.NoInternetConnection;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FavoritePostFragment extends Fragment {
	private ShimmerRecyclerView rvPopularPost;
	private PostAdapter postAdapter;
	private Context context;
	private String after="";
	private int limit=10;
	private ArrayList<PostModel> postModelArrayList=new ArrayList<>();
	private DatabaseHandler databaseHandler;
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment, container, false);
	}
	
	
	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);


		context  =getActivity();
		databaseHandler = new DatabaseHandler(context);
		postAdapter = new PostAdapter(context,postModelArrayList);
		rvPopularPost =(ShimmerRecyclerView) view.findViewById(R.id.recyclePopularPost);
		LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
		rvPopularPost.setLayoutManager(layoutManager);
		rvPopularPost.setItemAnimator(new DefaultItemAnimator());
		rvPopularPost.setAdapter(postAdapter);

		getPopularPost();

	}



	private void getPopularPost(){
		postModelArrayList.clear();
		postModelArrayList.addAll(databaseHandler.getAllFavorite());
		if(postModelArrayList.size() == 0){
			Toast.makeText(context,"No favorite found",Toast.LENGTH_SHORT).show();

		}
		postAdapter.notifyDataSetChanged();
	}
}
