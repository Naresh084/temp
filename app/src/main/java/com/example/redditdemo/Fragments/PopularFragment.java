package com.example.redditdemo.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.example.redditdemo.R;
import com.example.redditdemo.adapter.PostAdapter;
import com.example.redditdemo.listener.ScrollListener;
import com.example.redditdemo.model.PostModel;
import com.example.redditdemo.util.NetworkUtil;
import com.example.redditdemo.util.NoInternetConnection;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

public class PopularFragment extends Fragment implements ScrollListener.OnLoadMoreListener{
    private ShimmerRecyclerView rvPopularPost;
    private PostAdapter postAdapter;
    private Context context;
    private String after = "";
    private int limit = 10;
    private ArrayList<PostModel> postModelArrayList = new ArrayList<>();
    private SwipeRefreshLayout refreshLayout;
    private ScrollListener scrollListener;
    private boolean firstTime=true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        context = getActivity();
        postAdapter = new PostAdapter(context, postModelArrayList);
        rvPopularPost = (ShimmerRecyclerView) view.findViewById(R.id.recyclePopularPost);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rvPopularPost.setLayoutManager(layoutManager);
        rvPopularPost.setItemAnimator(new DefaultItemAnimator());
        scrollListener = new ScrollListener(layoutManager, this);
        scrollListener.setLoaded();
        rvPopularPost.addOnScrollListener(scrollListener);
        rvPopularPost.showShimmerAdapter();
        refreshLayout  = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                after = "";
                limit = 10;
                firstTime = true;
                rvPopularPost.setAdapter(null);
                postModelArrayList.clear();
                rvPopularPost.showShimmerAdapter();
                rvPopularPost.addOnScrollListener(scrollListener);
                scrollListener.setLoaded();
                getPopularPost();
            }
        });
        getPopularPost();

    }


    private void getPopularPost() {
        if (NetworkUtil.isOnline(context)) {
            AndroidNetworking.get(context.getResources().getString(R.string.base_url) + context.getResources().getString(R.string.popular_post))
                    .addQueryParameter("limit", String.valueOf(limit))
                    .addQueryParameter("after", after)
                    .setPriority(Priority.MEDIUM)

                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (!firstTime) {
                                    postModelArrayList.remove(postModelArrayList.size() - 1);
                                }
                                JSONObject dataObject = response.getJSONObject("data");
                                after = dataObject.isNull("after") ? " " : dataObject.getString("after");
                                JSONArray childrenArray = dataObject.getJSONArray("children");
                                for (int i = 0; i < childrenArray.length(); i++) {
                                    JSONObject data = childrenArray.getJSONObject(i).getJSONObject("data");
                                    PostModel postModel = new Gson().fromJson(data.toString(), PostModel.class);
                                    postModelArrayList.add(postModel);
                                }
                                if (!firstTime){
                                    postAdapter.notifyDataSetChanged();
                                }else {
                                    rvPopularPost.hideShimmerAdapter();
                                    rvPopularPost.setAdapter(postAdapter);
                                    postAdapter.notifyDataSetChanged();

                                }
                                scrollListener.setLoaded();
                                firstTime = false;
                                refreshLayout.setRefreshing(false);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            refreshLayout.setRefreshing(false);

                        }
                    });
        } else {
            Intent i = new Intent(context, NoInternetConnection.class);
            startActivity(i);
        }
    }

    @Override
    public void onLoadMore() {
        postModelArrayList.add(null);
        rvPopularPost.post(new Runnable() {
            public void run() {
                // There is no need to use notifyDataSetChanged()
                postAdapter.notifyItemInserted(postModelArrayList.size() - 1);
            }
        });
        getPopularPost();
    }
}
