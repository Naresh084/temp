package com.example.redditdemo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class SplashScreen extends Activity {

    private ImageView img_app;
    private TextView tv_appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        // Todo :- Animation call here
        StartAnimations();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Todo :- Animation here

                    Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(intent);
                    finish();
            }
        }, 3000);
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relative);
        relativeLayout.clearAnimation();
        relativeLayout.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();

        img_app = (ImageView) findViewById(R.id.img_app);
        img_app.clearAnimation();
        img_app.startAnimation(anim);

        tv_appName = (TextView) findViewById(R.id.tv_appName);
        tv_appName.clearAnimation();
        tv_appName.startAnimation(anim);
    }
}