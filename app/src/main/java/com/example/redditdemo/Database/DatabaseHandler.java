package com.example.redditdemo.Database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.redditdemo.model.PostModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "DB_REDDIT";

    /*Valve table*/
    private static final String TABLE_FAVORITE = "favorite";
    private static final String KEY_ID = "id";
    private static final String KEY_UNIQUE_ID = "unique_id";
    private static final String KEY_NAME = "data";
    private static final String KEY_NOTIFY_TIME = "time";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            String CREATE_FAVORITE_TABLE = "CREATE TABLE " + TABLE_FAVORITE + "("
                    + KEY_ID + " INTEGER PRIMARY KEY,"
                    + KEY_UNIQUE_ID + " TEXT ,"
                    + KEY_NAME + " TEXT ,"+KEY_NOTIFY_TIME + " TEXT" + ")";
            db.execSQL(CREATE_FAVORITE_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVORITE);

            // Create tables again
            onCreate(db);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // code to add the new valveModel
    public boolean addFavorite(PostModel postModel) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_UNIQUE_ID, postModel.getId()); // Area Name
            values.put(KEY_NAME, new Gson().toJson(postModel)); // Area Phone

            Calendar c = Calendar.getInstance();
            System.out.println("Current time =&gt; " + c.getTime());

            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String formattedDate = df.format(c.getTime());
            String[] splitArray = formattedDate.split(" ");
            formattedDate = splitArray[0];

            String[] temp = splitArray[1].split(":");
            if (Integer.parseInt(temp[0]) < 12) {
                formattedDate = formattedDate + " " + splitArray[1] + " AM";
            } else {
                int hour = Integer.parseInt(temp[0]) - 12;
                formattedDate = formattedDate + " " + hour + ":" + temp[1] + " PM";
            }

            values.put(KEY_NOTIFY_TIME, formattedDate);

            // Inserting Row
            if (getFavoriteById(postModel.getId()) == null) {
                db.insert(TABLE_FAVORITE, null, values);
                return true;
            } else {
                db.close();
                return false;// Closing database
            }
            //2nd argument is String containing nullCo
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    // code to get the single area
    public PostModel getFavoriteById(String id) {
        Cursor cursor = null;
        int count = 0;
        PostModel postModel = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();

            cursor = db.query(TABLE_FAVORITE, new String[]{KEY_ID,
                            KEY_UNIQUE_ID,KEY_NAME,  KEY_NOTIFY_TIME}, KEY_UNIQUE_ID + "=?",
                    new String[]{id}, null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();

            if (cursor != null && cursor.getCount() > 0) {
                postModel = new PostModel();
                int dbid = cursor.getInt(0);
                String dbId2 = cursor.getString(1);
                postModel = new Gson().fromJson(cursor.getString(2),PostModel.class);
                postModel.setId(dbId2);
                postModel.setDbId(dbid);
                postModel.setDbTime(cursor.getString(3));

                count = cursor.getCount();
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // return Valve
        return count > 0 ? postModel : null;
    }

    // code to get all valve in a list view
    public ArrayList<PostModel> getAllFavorite() {
        ArrayList<PostModel> postModelList = null;
        try {
            postModelList = new ArrayList<PostModel>();
            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_FAVORITE;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    PostModel postModel = new PostModel();
                    int dbid = cursor.getInt(0);
                    String dbId2 = cursor.getString(1);
                    postModel = new Gson().fromJson(cursor.getString(2),PostModel.class);
                    postModel.setId(dbId2);
                    postModel.setDbId(dbid);
                    postModel.setDbTime(cursor.getString(3));
                    postModel.setFavorite(true);
                    // Adding valveModel to list
                    postModelList.add(postModel);
                } while (cursor.moveToNext());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        // return Valve list
        return postModelList;
    }


    // Deleting single Valve
    public int deleteFavorite(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int count = db.delete(TABLE_FAVORITE, KEY_UNIQUE_ID + " = ?",
                new String[]{id});
        db.close();
        return count;
    }

}