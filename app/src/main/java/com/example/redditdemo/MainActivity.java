package com.example.redditdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.example.redditdemo.Fragments.FavoritePostFragment;
import com.example.redditdemo.Fragments.HotPostFragment;
import com.example.redditdemo.Fragments.PopularFragment;
import com.example.redditdemo.Fragments.RisingPostFragment;
import com.example.redditdemo.adapter.PostAdapter;
import com.example.redditdemo.model.PostModel;
import com.example.redditdemo.util.NetworkUtil;
import com.example.redditdemo.util.NoInternetConnection;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.bottomNav);

        loadFragment(new PopularFragment());               //Default is home fragment


        onClick();
    }

    private boolean loadFragment(Fragment fragment) {

        if (fragment != null) {
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            return true;
        }
        return false;
    }


    public void onClick() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.popular:
                        return loadFragment(new PopularFragment());
                    case R.id.hot:
                        return loadFragment(new HotPostFragment());
                    case R.id.rising:
                        return loadFragment(new RisingPostFragment());
                    case R.id.favorite:
                        return loadFragment(new FavoritePostFragment());
                }

                return false;
            }
        });
    }
}
