package com.example.redditdemo;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.redditdemo.util.ProgressIndicator;


public class WebActivity extends AppCompatActivity {

    private LinearLayout ll_back;
    private TextView tv_toolbar_title;
    private WebView webView;
    private String link;
    private String title;
    private RelativeLayout relativeLayout;
    private ProgressIndicator progressIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        link = getIntent().getStringExtra("link");
        title = getIntent().getStringExtra("title");

        // Todo :- Initialization
        init();

        // Todo :- onClick
        onClick();
    }

    private void init() {
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeBackground);
        ll_back.setVisibility(View.VISIBLE);
        tv_toolbar_title = (TextView) findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(title);
        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.loadUrl(link);
        if(getIntent().getBooleanExtra("flag",false)){
            relativeLayout.setBackgroundColor(getResources().getColor(R.color.white));
        }
        progressIndicator = new ProgressIndicator(this);
        progressIndicator.showProgress();
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                progressIndicator.stopProgress();
            }
        });
    }

    private void onClick() {
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}