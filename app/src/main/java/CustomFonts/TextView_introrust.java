package CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextView_introrust extends TextView {

    public TextView_introrust(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextView_introrust(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextView_introrust(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/introrustg-base2line.otf");
            setTypeface(tf);
        }
    }
}